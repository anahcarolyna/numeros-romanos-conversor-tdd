package br.com.romanos;

public class NumerosRomanos {

    public String converterDecimalParaRomano(int numero ) {
        try {
            if(numero >= 1 && numero < 4000) {
                return ordenarNumeros(gerarBase(numero));
            }
            else{
                throw new RuntimeException("O número a ser convertido vai de 1 à 3999");
            }
        }
        catch (RuntimeException ex){
           throw ex;
        }
    }

    private static String gerarBase(int numero) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 1000, 'M'));
        numero %= 1000;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 500, 'D'));
        numero %= 500;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 100, 'C'));
        numero %= 100;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 50, 'L'));
        numero %= 50;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 10, 'X'));
        numero %= 10;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 5, 'V'));
        numero %= 5;

        stringBuilder.append(gerarNumeroBaseDivisao(numero, 1, 'I'));

        return stringBuilder.toString();

    }

    private static String ordenarNumeros(String numero) {
        StringBuilder stringBuilder = new StringBuilder();

        int quantidadeM = 0;
        int quantidadeD = 0;
        int quantidadeC = 0;
        int quantidadeL = 0;
        int quantidadeX = 0;
        int quantidadeV = 0;
        int quantidadeI = 0;

        for (char valor : numero.toCharArray()) {
            if (valor == 'M') {
                quantidadeM++;
            } else if (valor == 'D') {
                quantidadeD++;
            } else if (valor == 'C') {
                quantidadeC++;
            } else if (valor == 'L') {
                quantidadeL++;
            } else if (valor == 'X') {
                quantidadeX++;
            } else if (valor == 'V') {
                quantidadeV++;
            } else if (valor == 'I') {
                quantidadeI++;
            }
        }

        boolean pularProximoNumero = false;

        if (quantidadeI == 4) {
            if (quantidadeV == 1) {
                stringBuilder.append("XI");
                pularProximoNumero = true;
            } else {
                stringBuilder.append("VI");
                pularProximoNumero = true;
            }
        } else {
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeI, 'I'));
            pularProximoNumero = false;
        }

        if (!pularProximoNumero) {
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeV, 'V'));
        }

        if (quantidadeX == 4) {
            if (quantidadeL == 1) {
                stringBuilder.append("CX");
                pularProximoNumero = true;
            } else {
                stringBuilder.append("LX");
                pularProximoNumero = true;
            }
        } else {
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeX, 'X'));
            pularProximoNumero = false;
        }

        if(!pularProximoNumero){
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeL, 'L'));
        }

        if(quantidadeC == 4) {
            if (quantidadeD == 1) {
                stringBuilder.append("MC");
                pularProximoNumero = true;
            } else {
                stringBuilder.append("DC");
                pularProximoNumero = true;
            }
        }else{
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeC, 'C'));
            pularProximoNumero=false;
        }

        if(!pularProximoNumero){
            stringBuilder.append(retornarQuantidadeSimbolo(quantidadeD, 'D'));
        }

        stringBuilder.append(retornarQuantidadeSimbolo(quantidadeM, 'M'));


        return stringBuilder.reverse().toString();

    }

    private static String gerarNumeroBaseDivisao(int valor, int numeroDivisor, char simbolo){
         return retornarQuantidadeSimbolo(valor/ numeroDivisor, simbolo);
    }

    private static String retornarQuantidadeSimbolo(int quantidade, char simbolo){
        StringBuilder stringBuilder = new StringBuilder();

        for(int contador = 0; contador < quantidade; contador++){
            stringBuilder.append(simbolo);
        }

        return stringBuilder.toString();
    }
}
