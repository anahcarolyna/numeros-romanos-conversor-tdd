package br.com.romanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NumerosRomanosTeste {

    private NumerosRomanos numeroRomano;

    @BeforeEach
    public void setUp(){
        numeroRomano = new NumerosRomanos();
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaUnidade() {
        Assertions.assertEquals("IX", numeroRomano.converterDecimalParaRomano(9));
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaDezenas() {
        Assertions.assertEquals("LXXXV", numeroRomano.converterDecimalParaRomano(85));
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaCentenas() {
        Assertions.assertEquals("CCXLVII", numeroRomano.converterDecimalParaRomano(247));
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaMilhar() {
        Assertions.assertEquals("MMDXCIX", numeroRomano.converterDecimalParaRomano(2599));
    }

    @Test
    public void testarConversaoDecimalParaRomanosNoLimite() {
        Assertions.assertEquals("MMMCMXCIX", numeroRomano.converterDecimalParaRomano(3999));
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaNumeroMenorQueUm() {
        Assertions.assertThrows(RuntimeException.class, () -> {numeroRomano.converterDecimalParaRomano(0);});
    }

    @Test
    public void testarConversaoDecimalParaRomanosParaAcimaDoLimiteMaximo() {
        Assertions.assertThrows(RuntimeException.class, () -> {numeroRomano.converterDecimalParaRomano(4000);});
    }
}